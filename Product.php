<?php
class Product
{

    protected $name = '';
    protected $tag = '';
    protected $price = 0;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct($name, $price, $tag)
    {
        $this->name = $name;
        $this->price = $price;
        $this->tag = $tag;

    }

    /**
     * getName 取得名稱
     *
     * @return void
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * getPrice 取得單價
     *
     * @return void
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * getTag 取得活動標籤 (R 為紅標， G 為綠標)
     *
     * @return void
     */
    public function getTag()
    {
        return $this->tag;
    }
}
