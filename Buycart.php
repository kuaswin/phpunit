<?php

class Buycart
{
    private $products = [
        'R' => [],
        'G' => []
    ];

    private $total = 0;

    /**
     * addProduct 加入購物車(接受單一商品物件)
     *
     * @return void
     */
    public function addProduct(Product $p)
    {
        $this->products[$p->getTag()] = $p;
    }

    /**
     * checkout 結帳(計算總價，成功回傳 true，失敗回傳錯誤訊息)
     *
     * @return true | error message
     */
    public function checkout()
    {
        $red_count = count($this->products['R']);
        $green_count = count($this->products['G']);

        // 比對紅標商品數是否等於綠標商品數量
        if ($red_count !== $green_count) {
            throw new BuycartException('商品配對錯誤');
        }

        // 計算金額
        foreach ($this->products as $product) {
            $this->total += $product->getPrice();
        }

        $this->total *= 0.75;
    }

    /**
     * getTotal 取得最後的總價
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }
}

class BuycartException extends Exception
{
}
