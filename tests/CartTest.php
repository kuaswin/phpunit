<?php

class CartTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provider
     * @group update
     */
    public function testUpdateQuantitesAndGetToal($quntities, $expected)
    {
        $cart = new Cart();

        $cart->updateQuantities($quntities);
        $this->assertEquals($expected, $cart->getTotal());

        return $cart;

    }

    public function provider()
    {
        return [
            [[1, 0, 0, 0, 0, 0], 199 + 20],
            [[0, 0, 0, 0, 0, 0], 0],
            [[1, 0, 0, 2, 0, 0], 797],
        ];
    }

    /**
     * @group get
     */
    public function testGetProducts()
    {
        $cart = new Cart();
        $products = $cart->getProducts();
        $this->assertEquals(7, count($products));
        $this->assertEquals(0, $products[3]['quantity']);
    }

    /**
     * @expectedException CartException
     * @group update
     * @group exception
     */
    public function testUpdateQuantitiesWhiteException()
    {
        // $this->setExpectedException('CartException'); 不寫Doc 註解也可以下這行
        $cart = new Cart();
        $quantities = [-1, 0, 0, -2, 0, 0];
        $cart->updateQuantities($quantities);
    }
}
