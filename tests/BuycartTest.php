<?php

class BuycartTest extends PHPUnit_Framework_TestCase
{
    private $cart = null;
    private $products = [];

    public function setUp() {
        $this->buycart = new Buycart();
        $this->products = [
            new Product('紅標1', 200, 'R'),
            new Product('紅標2', 160, 'R'),
            new Product('綠標1', 80, 'G'),
            new Product('綠標2', 100, 'G')
        ];
    }

    public function tearDown()
    {
        $this->buycart = null;
        $this->products = [];
    }

    /**
     * testAddOneRedProduct 加入一個紅標商品的測試案例
     *
     */
    public function testAddOneRedProduct()
    {
         $this->setExpectedException('BuycartException', '商品配對錯誤');
         $this->buycart->addProduct($this->products[0]); // 紅標商品 1
         $this->buycart->checkout();
    }

    /**
     * testAddOneRedProductAndOneGreenProduct 加入一個紅標和一個綠標商品測試案例
     *
     * @return void
     */
    public function testAddOneRedProductAndOneGreenProduct()
    {
        $this->buycart->addProduct($this->products[0]);
        $this->buycart->addProduct($this->products[2]);
        $this->buycart->checkout();
        $this->assertEquals(210, $this->buycart->getTotal()); // (200+80) * .75 = 210
    }

    /**
     * testAddTwoRedProductAndTwoGreenProduct 加入紅標2和綠標2商品測試案例
     *
     */
    public function testAddTwoRedProductAndTwoGreenProduct()
    {
        $this->buycart->addProduct($this->products[1]);
        $this->buycart->addProduct($this->products[3]);
        $this->buycart->checkout();
        $this->assertEquals(195, $this->buycart->getTotal()); // (160+100) * .75 = 195
    }
}
